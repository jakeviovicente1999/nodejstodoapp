const express = require("express");
const mongoose = require('mongoose');

const uri = "mongodb+srv://jakev:admin123@cluster0.33reu9d.mongodb.net/training_db?retryWrites=true&w=majority";

const app = express();

async function connect() {
    try {
        await mongoose.connect(uri, { useNewUrlParser: true, useUnifiedTopology: true });
        console.log("Connected to MongoDB");
    } catch (error) {
        console.error(error);
    }
}

connect();
