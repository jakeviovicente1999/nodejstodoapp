require('dotenv').config();
require("./config/mongodb.config");
const express = require("express");
const cors = require("cors");
const path = require("path");
const { logger } = require("./app/middlewares/logging.middleware");
const app = express();


app.use(express.json());

app.use(
    cors({
        origin: "*",
    })
);

app.get("/flexbook-master");
// api
app.use(express.static(path.join(__dirname, "FE", "flexbook-master")));
app.use('/uploads', express.static(path.join(__dirname, 'uploads')));



require("./app/routes/user.routes")(app);
require("./app/routes/comment.routes")(app);
require("./app/routes/sample.routes")(app);
require("./app/routes/createPost.routes")(app);
require("./app/routes/chat.routes")(app);

const port = process.env.APP_PORT || 8080;
app.listen(port, () => logger.info(`Listening on port ${port}`));
