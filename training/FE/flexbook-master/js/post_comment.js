
function buildCommentHTML(comment) {
    return `
    <div class="accordion-body" data-post-id="${comment.id}">
                    <p>${comment.comment} </p>            
    </div >
    `;
}

function fetchCommentData() {
    $.ajax({
        url: `http://localhost:8080/api/comment`,
        method: "GET",
        success: function (response) {
            if (response.data && Array.isArray(response.data)) {
                response.data.forEach((comment) => {
                    const commentHTML = buildCommentHTML(comment);
                    $("#commentsContainer").append(commentHTML);

                });
            } else {
                console.error(
                    "Expected an array of comment in the data field, but got:",
                    response
                );
            }
        },
        error: function (err) {
            console.error("Failed to fetch posts:", err);
        },
    });
}


