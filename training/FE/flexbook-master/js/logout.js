// log out
$(document).ready(function () {
    $("#logoutButton").click(function (event) {
        event.preventDefault();

        $.ajax({
            url: "http://localhost:8080/api/users/logout",
            method: "POST",
            headers: {
                Authorization: `Bearer ${token}`,
            },
            success: function (response) {

                window.location.href = "index.html";
            },
        });
    });
});
