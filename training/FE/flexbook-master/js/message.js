$(document).ready(function () {
    $("#messageForm").submit(function (event) {
        event.preventDefault();
        const userId = localStorage.getItem("userId");
        const postData = {
            sender: userId,
            receiver: $("#receiver").val(),
            message: $("#message").val(),
        };
        console.log(postData);

        $.ajax({
            url: "http://localhost:8080/api/chat",
            method: "POST",
            headers: {
                Authorization: `Bearer ${token}`,
            },
            contentType: "application/json",
            data: JSON.stringify(postData),

            success: function (response) {
                console.log("Message sent successfully:", response);
                window.location.reload();
            },
            error: function (error) {
                console.error("Failed to send message:", error);
                alert("Failed to send message. Please check your input.");
            },
        });
    });
});
