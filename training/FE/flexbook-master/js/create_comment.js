function buildFormHTML(post) {
    return `
    
    <form class="d-flex my-1 comment-form">
    <div>
      <img
        src="https://source.unsplash.com/collection/happy-people"
        alt="avatar"
        class="rounded-circle me-2"
        style="width: 38px; height: 38px; object-fit: cover"
      />
    </div>
    <input id="post_id" name="post_id" value="${post._id}" hidden />
    <input
      id="comment_des"
      name="comment_des"
      type="text"
      class="form-control border-0 rounded-pill bg-gray"
      placeholder="Write a comment"
    />
    <button type="submit" class="btn btn-primary w-100">Send</button>
  </form>
    `;
}

function fetchFormData() {

    $.ajax({
        url: `http://localhost:8080/api/create-post`,
        method: "GET",
        headers: {
            Authorization: `Bearer ${token}`,
        },
        success: function (response) {
            if (response.data && Array.isArray(response.data)) {
                response.data.forEach((post) => {
                    const postHTML = buildFormHTML(post);
                    $("#createComment").append(postHTML);

                });
            } else {
                console.error(
                    "Expected an array of posts in the data field, but got:",
                    response
                );
            }
        },
        error: function (err) {
            console.error("Failed to fetch posts:", err);
        },
    });
}

// Create Comment
$(document).on("submit", ".comment-form", function (event) {
    event.preventDefault();

    const comData = {
        post_id: $("#post_id").val(),
        comment: $("#comment_des").val(),
    };

    $.ajax({
        url: 'http://localhost:8080/api/comment',
        method: "POST",
        headers: {
            Authorization: `Bearer ${token}`,
        },
        data: JSON.stringify(comData),
        dataType: 'json',
        contentType: 'application/json',
        success: function (response) {
            console.log("Commented successfully:", response);
            alert("Success");
        },
        error: function (error) {
            console.log(comData);
            console.error("Comment Creation Error:", error);
            alert("Failed to create comment. Please check your input.");
        },
    });
});