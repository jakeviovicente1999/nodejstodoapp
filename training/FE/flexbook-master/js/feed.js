const token = localStorage.getItem("token");
// create post 
$(document).ready(function () {
    $("#createPostForm").submit(function (event) {
        event.preventDefault();
        // const userId = localStorage.getItem("userId");
        const userId = localStorage.getItem("userId");
        const userIdInput =
            document.getElementById("user_id");
        userIdInput.value = userId;
        const postData = new FormData(this);
        // postData.append("user_id", userId);
        // console.log(userId);
        console.log(postData);
        $.ajax({
            url: "http://localhost:8080/api/create-post",
            method: "POST",
            headers: {
                Authorization: `Bearer ${token}`,
            },
            processData: false,
            contentType: false,
            data: postData,

            success: function (response) {
                console.log("Post Creation Success:", response);
                window.location.reload();
            },
            error: function (error) {
                console.error("Post Creation Error:", error);
                alert("Failed to create post. Please check your input.");
            },
        });
    });
});

// <!--get uploaded-- >
function buildPostHTML(post) {
    const imageHTML = post.image ? `<img src="${post.image}" alt="post image" class="img-fluid rounded" />` : '';
    // const commentsHTML = post.comments.map(comment => buildCommentHTML(comment)).join('');

    return `
        <div class="bg-white p-4 rounded shadow mt-3" data-post-id="${post.id}">
            <div class="d-flex justify-content-between">
                <div class="d-flex">
                    <img src="https://source.unsplash.com/collection/happy-people" alt="avatar" class="rounded-circle me-2" style="width: 38px; height: 38px; object-fit: cover" />
                    <div>
                        <p class="m-0 fw-bold">Jake</p>
                        <span class="text-muted fs-7">${post.allow}</span>
                    </div>
                </div>
                <!-- edit -->
                <i
                    class="fas fa-ellipsis-h"
                    type="button"
                    id="post1Menu"
                    data-bs-toggle="dropdown"
                    aria-expanded="false"
                ></i>
                <!-- edit menu -->
                <ul
                    class="dropdown-menu border-0 shadow"
                    aria-labelledby="post1Menu"
                >
                    <li class="d-flex align-items-center">
                        <a
                            class="
                                dropdown-item
                                d-flex
                                justify-content-around
                                align-items-center
                                fs-7
                            "
                            href="#"
                        >
                            Edit Post</a
                        >
                    </li>
                    <li class="d-flex align-items-center">
                        <!-- Pass the post's ID as a data attribute -->
                        <button
                            class="
                                dropdown-item
                                d-flex
                                justify-content-around
                                align-items-center
                                fs-7
                            "
                            data-post-id="${post._id}" 
                            id="deletebtn"
                        >
                            Delete Post</button>
                    </li>
                </ul>

            </div>
            <div class="mt-3">
                <div id="postContent">
                    <p>${post.post_description}</p>
                    ${imageHTML}
                </div>





                <!-- likes & comments -->
                <div class="post__comment mt-3 position-relative">
                  <!-- likes -->
                  <div
                    class="
                      d-flex
                      align-items-center
                      top-0
                      start-0
                      position-absolute
                    "
                    style="height: 50px; z-index: 5"
                  >
                    <div class="me-2">
                      <i class="text-primary fas fa-thumbs-up"></i>
                      <i class="text-danger fab fa-gratipay"></i>
                      <i class="text-warning fas fa-grin-squint"></i>
                    </div>
                    <p class="m-0 text-muted fs-7">Phu, Tuan, and 3 others</p>
                  </div>
                  <!-- comments start-->
                  <div class="accordion" id="accordionExample">
                    <div class="accordion-item border-0">
                      <!-- comment collapse -->
                      <h2 class="accordion-header" id="headingTwo">
                        <div
                          class="
                            accordion-button
                            collapsed
                            pointer
                            d-flex
                            justify-content-end
                          "
                          data-bs-toggle="collapse"
                          data-bs-target="#collapsePost1"
                          aria-expanded="false"
                          aria-controls="collapsePost1"
                        >
                          <p class="m-0">2 Comments</p>
                        </div>
                      </h2>
                      <hr />
                      <!-- comment & like bar -->
                      <div class="d-flex justify-content-around">
                        <div
                          class="
                            dropdown-item
                            rounded
                            d-flex
                            justify-content-center
                            align-items-center
                            pointer
                            text-muted
                            p-1
                          "
                        >
                          <i class="fas fa-thumbs-up me-3"></i>
                          <p class="m-0">Like</p>
                        </div>
                        <div
                          class="
                            dropdown-item
                            rounded
                            d-flex
                            justify-content-center
                            align-items-center
                            pointer
                            text-muted
                            p-1
                          "
                          data-bs-toggle="collapse"
                          data-bs-target="#collapsePost1"
                          aria-expanded="false"
                          aria-controls="collapsePost1"
                        >
                          <i class="fas fa-comment-alt me-3"></i>
                          <p class="m-0">Comment</p>
                        </div>
                      </div>
                      <!-- comment expand -->
                      <div
                        id="collapsePost1"
                        class="accordion-collapse collapse"
                        aria-labelledby="headingTwo"
                        data-bs-parent="#accordionExample"
                      >
                        <hr />
                        
                        <div class="accordion-body" id="comments-container">
                      
                        </div>
                        

                          <!-- create comment -->
                          <form class="d-flex my-1 comment-form">
                                <div>
                            <img
                                src="https://source.unsplash.com/collection/happy-people"
                                alt="avatar"
                                class="rounded-circle me-2"
                                style="width: 38px; height: 38px; object-fit: cover;"
                            />
                            </div>
                            <input 
                    id="post_id"
                        name="post_id"
                        value="${post._id}"
                        hidden
                    />
                    <input
                    id="comment_des"
                                name="comment_des"
                                type="text"
                                class="form-control border-0 rounded-pill bg-gray"
                                placeholder="Write a comment"
                            />
                            <button type="submit" class="btn btn-primary w-100">Send</button>
                        </form>

                          <!-- end -->
                        </div>
                      </div>
                    </div>
                  </div>
                  <!-- end -->
                </div>
            </div>

    `;
}
// function buildCommentHTML(comment) {
//     return `
//     <div class="accordion-body" data-post-id="${comment.id}">
//         <div class="comments-container" >
//           <p>${comment.comment} </p>
//         </div>
//     </div >
//     `;
// }

function fetchPostsData() {

    $.ajax({
        url: `http://localhost:8080/api/create-post`,
        method: "GET",
        headers: {
            Authorization: `Bearer ${token}`,
        },
        success: function (response) {
            if (response.data && Array.isArray(response.data)) {
                response.data.forEach((post) => {
                    const postHTML = buildPostHTML(post);
                    $("#postContainer").append(postHTML);

                });
            } else {
                console.error(
                    "Expected an array of posts in the data field, but got:",
                    response
                );
            }
        },
        error: function (err) {
            console.error("Failed to fetch posts:", err);
        },
    });
}

// delete post 
$(document).ready(function () {
    $(document).on("click", "#deletebtn", function (event) {
        event.preventDefault();

        const postId = $(this).data("post-id");

        $.ajax({
            url: `http://localhost:8080/api/create-post/${postId}`,
            method: "DELETE",
            headers: {
                Authorization: `Bearer ${token}`,
            },
            contentType: "application/json",
            success: function (response) {
                console.log("Post Deleted:", response);
                window.location.reload();
            },
            error: function (error) {
                console.error("Post Delition Error:", error);
                alert("Failed to delete post");
            },
        });
    });
});


// comment
$(document).on("submit", ".comment-form", function (event) {
    event.preventDefault();

    const comData = {
        post_id: $("#post_id").val(),
        comment: $("#comment_des").val(),
    };

    $.ajax({
        url: 'http://localhost:8080/api/comment',
        method: "POST",
        headers: {
            Authorization: `Bearer ${token}`,
        },
        data: JSON.stringify(comData),
        dataType: 'json',
        contentType: 'application/json',
        success: function (response) {
            console.log("Commented successfully:", response);
            alert("Success");
        },
        error: function (error) {
            console.log(comData);
            console.error("Comment Creation Error:", error);
            alert("Failed to create comment. Please check your input.");
        },
    });
});
//display comment
function fetchCommentData() {
    $.ajax({
        url: `http://localhost:8080/api/comment`,
        method: "GET",
        success: function (response) {
            if (response.data && Array.isArray(response.data)) {
                response.data.forEach((comment) => {
                    const commentHTML = buildCommentHTML(comment);
                    $("#comments-container").append(commentHTML);

                });
            } else {
                console.error(
                    "Expected an array of comment in the data field, but got:",
                    response
                );
            }
        },
        error: function (err) {
            console.error("Failed to fetch posts:", err);
        },
    });
}
// log out
$(document).ready(function () {
    $("#logoutButton").click(function (event) {
        event.preventDefault();

        $.ajax({
            url: "http://localhost:8080/api/users/logout",
            method: "POST",
            headers: {
                Authorization: `Bearer ${token}`,
            },
            success: function (response) {

                window.location.href = "index.html";
            },
        });
    });
});

$(document).ready(function () {
    fetchContactData();
    fetchPostsData();
    fetchCommentData();
});