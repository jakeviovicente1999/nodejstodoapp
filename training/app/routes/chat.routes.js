require('dotenv').config();

const defaultVariable = require('../controllers/chat.controller');
const pagination = require('../middlewares/pagination.middleware');
const auth = require('../middlewares/authorization.middleware');

module.exports = (app) => {
    app.post(process.env.BASE_URL + '/chat', auth.validateToken, defaultVariable.create);
    app.get(process.env.BASE_URL + '/chat', auth.validateToken, pagination.setAttributes, defaultVariable.search);
    app.get(process.env.BASE_URL + '/chat/:id', auth.validateToken, defaultVariable.read);
    app.put(process.env.BASE_URL + '/chat/:id', auth.validateToken, defaultVariable.update);
    app.delete(process.env.BASE_URL + '/chat/:id', auth.validateToken, defaultVariable.delete);
};
