require('dotenv').config();
const defaultVariable = require('../controllers/sample.controller');
const pagination = require('../middlewares/pagination.middleware');

module.exports = (app) => {
    app.post(process.env.BASE_URL + '/sample', defaultVariable.create);
    app.get(process.env.BASE_URL + '/sample', pagination.setAttributes, defaultVariable.search);
    app.get(process.env.BASE_URL + '/sample/:id', defaultVariable.read);
    app.put(process.env.BASE_URL + '/sample/:id', defaultVariable.update);
    app.delete(process.env.BASE_URL + '/sample/:id', defaultVariable.delete);
};
