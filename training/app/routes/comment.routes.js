require('dotenv').config();

const defaultVariable = require('../controllers/comment.controller');
const pagination = require('../middlewares/pagination.middleware');
const multerMiddleware = require('../middlewares/multer.middleware');

module.exports = (app) => {
    app.post(process.env.BASE_URL + '/comment', defaultVariable.create);
    app.get(process.env.BASE_URL + '/comment', pagination.setAttributes, defaultVariable.search);
    app.get(process.env.BASE_URL + '/comment/:id', defaultVariable.read);
    app.put(process.env.BASE_URL + '/comment/:id', defaultVariable.update);
    app.delete(process.env.BASE_URL + '/comment/:id', defaultVariable.delete);
};
