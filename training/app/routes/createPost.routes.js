require('dotenv').config();

const defaultVariable = require('../controllers/createPost.controller');
const pagination = require('../middlewares/pagination.middleware');
const multerMiddleware = require('../middlewares/multer.middleware');
const auth = require('../middlewares/authorization.middleware');

module.exports = (app) => {
    app.post(process.env.BASE_URL + '/create-post', auth.validateToken, multerMiddleware.upload.single('image'), defaultVariable.create);
    app.get(process.env.BASE_URL + '/create-post', auth.validateToken, pagination.setAttributes, defaultVariable.search);
    app.get(process.env.BASE_URL + '/create-post/:id', auth.validateToken, defaultVariable.read);
    app.put(process.env.BASE_URL + '/create-post/:id', auth.validateToken, defaultVariable.update);
    app.delete(process.env.BASE_URL + '/create-post/:id', auth.validateToken, defaultVariable.delete);
};
