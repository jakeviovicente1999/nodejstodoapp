const Joi = require('joi');

const LIMIT_DEFAULT_CHAR = 128;

const defaultSchema = Joi.object({
    first_name: Joi.string().trim().required().max(LIMIT_DEFAULT_CHAR),
    last_name: Joi.string().trim().required().max(LIMIT_DEFAULT_CHAR),
    position: Joi.string().trim().required().max(LIMIT_DEFAULT_CHAR),
});
module.exports = {
    createSchema: defaultSchema,
    updateSchema: defaultSchema
};