const Joi = require('joi');

const LIMIT_DEFAULT_CHAR = 128;

const defaultSchema = Joi.object({
    user_id: Joi.string().trim().required().max(LIMIT_DEFAULT_CHAR),
    allow: Joi.string().trim().required().max(LIMIT_DEFAULT_CHAR),
    post_description: Joi.string().trim().required().max(LIMIT_DEFAULT_CHAR),
    image: Joi.string().trim().max(LIMIT_DEFAULT_CHAR).allow('', null),
});
module.exports = {
    createSchema: defaultSchema,
    updateSchema: defaultSchema
};