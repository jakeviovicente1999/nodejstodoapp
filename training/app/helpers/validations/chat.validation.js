const Joi = require('joi');

const LIMIT_DEFAULT_CHAR = 128;

const defaultSchema = Joi.object({
    sender: Joi.string().trim().required().max(LIMIT_DEFAULT_CHAR),
    receiver: Joi.string().trim().required().max(LIMIT_DEFAULT_CHAR),
    message: Joi.string().trim().required().max(LIMIT_DEFAULT_CHAR)
});
module.exports = {
    createSchema: defaultSchema,
    updateSchema: defaultSchema
};