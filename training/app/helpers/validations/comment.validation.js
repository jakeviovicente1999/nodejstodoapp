const Joi = require('joi');

const LIMIT_DEFAULT_CHAR = 128;

const defaultSchema = Joi.object({
    post_id: Joi.string().trim().required().max(LIMIT_DEFAULT_CHAR),
    comment: Joi.string().trim().required().max(LIMIT_DEFAULT_CHAR)
});
module.exports = {
    createSchema: defaultSchema,
    updateSchema: defaultSchema
};